МІНІСТЕРСТВО ОСВІТИ І НАУКИ УКРАЇНИ
ХАРКІВСЬКИЙ НАЦІОНАЛЬНИЙ ЕКОНОМІЧНИЙ УНІВЕРСИТЕТ
ІМЕНІ СЕМЕНА КУЗНЕЦЯ

ЗВІТ
з дисципліни «Сучасні Java-технології»
за  курсом «Spring Framework»

Виконав:
Студент 4 курсу
групи 6.04.122.010.19.3
факультету ІТ
Григоров М.В

Перевірив:
Фролов О.В.
Поляков А.О.

Харків – 2022

___

### URL - адреса для перевірки сертифікату
https://www.coursera.org/verify/G39Y2U4EQXSL

___

### Сертифікат

![](images/img1.png)
Рис.1. Картинка сертифікату о проходжені курсу Spring Framework

___

## Spring Ecosystem and Core

![](images/img2.png)
Рис.2. Картинка сертифікату о проходжені курсу Ecosystem and Core
___
![](images/img3.png)
Рис.3. Скриншот з профілю Coursera, що відображуе отриманий бал за курс

## Spring MVC, Boot, Rest Controllers
![](images/img4.png)
Рис.4. Картинка сертифікату о проходжені курсу Spring MVC, Boot, Rest

![](images/img5.png)
Рис.5. Скриншот з профілю Coursera, що відображуе отриманий бал за курс

___

## Spring Data Repositories

![](images/img6.png)
Рис.6. Картинка сертифікату о проходжені курсу Spring Data Repositories

![](images/img7.png)
Рис.7. Скриншот з профілю Coursera, що відображуе отриманий бал за курс

## Spring Cloud Overview

![](images/img8.png)
Рис.8. Картинка сертифікату о проходжені курсу Spring Cloud Overview

![](images/img9.png)
Рис.9. Скриншот з профілю Coursera, що відображуе отриманий бал за курс
___